import os
import random
import re

import discord

from dotenv import load_dotenv
load_dotenv()

TOKEN = os.getenv('ROSS_TOKEN')

client = discord.Client()

@client.event
async def on_ready():
	print(f'{client.user.name} has connected to Discord!')

@client.event
async def on_message(message):
	if message.author == client.user:
		return
	else:
		patternParent = re.compile("\!tarot")
		patternBall = re.compile("\!tarot magiceightball")
		if(patternParent.match(message.content)):
			if(message.content == '!tarot test'):
				response = "Tarot bot is responding to your messages!"
				await message.channel.send(response)
			elif (patternBall.match(message.content)):
				response = magicEightBall()
				await message.channel.send(response)
			else:
				response = "Available commands: test, magiceightball"
				await message.channel.send(response)


def magicEightBall():
    replies = [
        "As I see it, yes.",
        "Ask again later.",
        "Better not tell you now.",
        "Cannot predict now.",
        "Concentrate and ask again.",
        "Don’t count on it.",
        "It is certain.",
        "It is decidedly so.",
        "Most likely.",
        "My reply is no.",
        "My sources say no.",
        "Outlook not so good.",
        "Outlook good.",
        "Reply hazy, try again.",
        "Signs point to yes.",
        "Very doubtful.",
        "Without a doubt.",
        "Yes.",
        "Yes – definitely.",
        "You may rely on it."
	]

    response = random.choice(replies)

    return response


client.run(TOKEN)
