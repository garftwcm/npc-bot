import os
import random
import re

import discord

from dotenv import load_dotenv
load_dotenv()

TOKEN = os.getenv('DISCORD_TOKEN')

client = discord.Client()

@client.event
async def on_ready():
	print(f'{client.user.name} has connected to Discord!')

@client.event
async def on_message(message):
	if message.author == client.user:
		return
	else:
		patternParent = re.compile("\!npc")
		patternBond = re.compile("\!npc bond")
		if(patternParent.match(message.content)):
			if(message.content == '!npc test'):
				response = "NPC Bot is responding to your messages!"
				await message.channel.send(response)
			elif(message.content == '!npc taliesin'):
				response = taliesin()
				await message.channel.send(response)
			elif(patternBond.match(message.content)):
				localString = message.content
				separatedStrings = localString.split()
				if(len(separatedStrings) >= 3):
					playerOne = message.author.display_name
					playerTwo = separatedStrings[2]
					response = bond(playerOne,playerTwo)
				else:
					response = bond()
				await message.channel.send(response)
			elif(message.content == '!npc campfire'):
				response = campfire()
				await message.channel.send(repsonse)
			else:
				response = "Available commands: test, taliesin, bond, campfire"
				await message.channel.send(response)


def taliesin():
	dwarves = [
		'doc',
		'grumpy'
		'happy',
		'sleepy',
		'bashful',
		'sneezy',
		'dopey'
	]

	voice = [
		'high',
		'middle',
		'low'
	]

	gender = [
		'male',
		'female'
	]

	response = "Gender: " + random.choice(gender) + "\r\n" + "Dwarf: " + random.choice(dwarves) + "\r\n" +  "Vocal tone: " + random.choice(voice)

	return response

def bond(playerOne = "", playerTwo = ""):
	response = ""

    #Ostensibly, these two arrays should stay in sync, although nothing bad should happen if they drift
	bondSimple = [
		'They owe me their life',
		'They saved my life',
		'They are my cousin',
		'They owe me gold',
		'They need my help',
		'They are the chosen one',
		'They are Eskimo siblings with me',
		'They once acquired toiletries for me in a delicate situation',
		'They once stole and ate my rations and blamed it on someone else',
		'They saved my bacon. Literally.',
		'I watched them once drop a chandelier on our enemies and ride the rope up',
		'They once got us kicked out of an inn',
		"They refuse to share a room at the inn with me and won't say why",
		'They have sworn me to secrecy about something I saw them do',
		'We met as children but we reconnected later',
		"They have bought me way more drinks than I've bought them",
		'They grew up in the same hometown with me',
		'They grew up in a rival town to mine',
		"They lost a crucial piece of equipment to me in a card game",
		"We often combine shelter-halves to make a bigger tent that we share",
		"They know the truth about a lie I told the rest of the party",
		"We went on a quest that neither of us will ever discuss",
		"A 3am discussion changed our friendship forever",
		"We both share the same musical guilty pleasures",
		"We share a language in common that isn't shared by the rest of the party",
		"We are in the second season of a multi-season long will-they-won't-they arc",
		"We are both on a quest for the same cultist diary - one to use it and one to destroy it"
	]

	bondComplex = [
		'%2 owes %1 their life',
		"%2 saved %1's life",
		"%2 is %1's cousin",
		'%2 owes %1 gold',
		'%2 need %1 help',
		'%1 knows that %2 is the chosen one',
		'%1 and %2 are Eskimo siblings',
		'%2 once acquired toiletries for %1 in a delicate situation',
		"%2 once stole and ate %1's rations and blamed it on someone else",
		"%2 saved %1's bacon. Literally.",
		'%1 watched %2 once drop a chandelier on our enemies and ride the rope up',
		'%2 once got %1 kicked out of an inn',
		"%2 refuses to share a room at the inn with %1 and won't say why",
		'%2 has sworn %1 to secrecy about something %1 saw them do',
		'%1 and %2 met as children but did not reconnect until much later',
		"%2 has bought %1 way more drinks than %1 has bought %2",
		'%2 grew up in the same hometown as %1',
		'%2 grew up in a rival town to %1',
		"%2 lost something of personal significance to %1 in a card game",
		"%1 and %2 often combines shelter-halves make a bigger tent that they share",
		"%2 knows the truth about a lie %1 told the rest of the party",
		"%2 and %1 went on a quest that neither of them will ever discuss",
		"A 3am discussion changed %1 and %2's friendship forever",
		"%2 and %1 both share the same musical guilty pleasures",
		"%2 and %1 share a language in common that isn't shared by the rest of the party",
		"%2 and %1 are in the second season of a multi-season long will-they-won't-they arc",
		"%2 and %1 are both on a quest for the same cultist diary - one to use it and one to destroy it"
	]
	#print("P1:" + str(playerOne))
	#print("P2:" + playerTwo)

	if(str(playerOne) != "" and playerTwo != ""):
		response = random.choice(bondComplex)
		response = response.replace("%1",str(playerOne)).replace("%2",playerTwo)
	else:
		response = random.choice(bondSimple)

	return response

def campfire():
	suites = [
		#hearts
		'a story of love',
		#clubs
		'a story of pain',
		#spades
		'a story of loss',
		#diamonds
		'a story of gain'
	]

	return random.choice(suites)


client.run(TOKEN)

